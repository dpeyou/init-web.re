//small animation for button, when clicked
type state = {transform: string};

type action =
  | Grow
  | Shrink;

[@react.component]
let make = /*props*/
    (
      ~alignItems="center",
      ~onClickAnimation: bool=true,
      ~background: string="none",
      ~border: string="none",
      ~borderRadius: string="4px",
      ~borderWidth: string="",
      ~bottom: string="initial",
      ~boxShadow: string="",
      ~className: string="",
      ~color: string="#eee",
      ~display: string="flex",
      ~flex: string="initial",
      ~flexDirection: string="",
      ~fontSize: string="1.05rem",
      ~height: string="initial",
      ~id: string="",
      ~justifyContent="center",
      ~margin: string="0",
      ~onClick: unit => unit=() => (),
      ~opacity: string="1",
      ~padding: string="0",
      ~pointerEvents: string="auto",
      ~position: string="initial",
      ~right: string="",
      ~tabIndex: int=0,
      ~textAlign: string="",
      ~textDecoration: string="",
      ~transform: string="",
      ~transition: string="125ms cubic-bezier(0.08, 0.68, 0, 0.85)",
      ~width: string="initial",
      ~zIndex: string="initial",
      ~children: React.element,
    ) => {
  //--------------------------
  //----------start of reducer
  let (state, dispatch) =
    React.useReducer(
      (_state, action) =>
        switch (action) {
        | Grow => {transform: "scale(1.15)"}
        | Shrink => {transform: "scale(1.0)"}
        },
      //initial state
      {transform: "scale(1.0)"},
    );
  //----------------------------
  //--------------end of reducer

  //------------------------
  //------------------layout
  <button
    className
    id
    onClick={_mouseClick =>
      onClickAnimation == true
        ? {
            /*start animation*/ dispatch(Grow);
            //set timeout so that Shrink is executed after Grow
            let _Shrink =
              Js.Global.setTimeout(
                () => dispatch(Shrink),
                //match with transition time of component
                125,
              );
            //end of timeout
            ();
          }
          |> /*after animation, trigger function assigned to onClick prop*/ onClick
        : /*else, no animation: just trigger the function assigned via onClick prop*/ ()
          |> onClick
    }
    style={ReactDOMRe.Style.make(
      ~alignItems,
      ~background,
      ~bottom,
      ~border,
      ~borderRadius,
      ~borderWidth,
      ~boxShadow,
      ~color,
      ~display,
      ~flex,
      ~flexDirection,
      ~fontSize,
      ~height,
      ~justifyContent,
      ~margin,
      ~opacity,
      ~padding,
      ~pointerEvents,
      ~position,
      ~right,
      ~textAlign,
      ~textDecoration,
      ~transform={
        /*check whether or not a transform value has been set*/
        transform == ""
          ? /*if it hasn't been set, use default animation*/ state.transform
          : /*else, use what it was set to*/ transform;
      },
      ~transition,
      ~width,
      ~zIndex,
      (),
    )}
    tabIndex
    type_="button">
    children
  </button>;
};
