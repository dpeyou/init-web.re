[@react.component]
let make = /*~props*/
    (
      ~background: string="rgb(97, 85, 85)",
      ~height: string="2.7rem",
      ~justifyContent="space-between",
      ~position: string="absolute",
      ~children: React.element=React.null,
    ) => {
  //------------------------
  //------------------layout
  <header
    id="Header"
    style={ReactDOMRe.Style.make(
      ~alignItems="center",
      ~background,
      ~boxShadow="rgb(38, 30, 29) 0px 0px 45px 3px",
      ~display="flex",
      ~height,
      ~left="0",
      ~justifyContent,
      ~padding="0 5%",
      ~position,
      ~right="0",
      ~top="0",
      ~whiteSpace="pre",
      ~zIndex="3",
      (),
    )}>
    children
  </header>;
};
