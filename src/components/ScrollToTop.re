open Helpers;
open Images;
open Types;

//raw javascript
let scrollToTop: string => unit =
  _id => [%bs.raw {|
      document.getElementById(_id).scrollTop = 0
    |}];

[@react.component]
let make = /*props*/ (~currentPage: page, ~display: string="none") => {
  //------------------------
  //------------------layout
  <Button
    background="rgba(91, 85, 85, 1.0)"
    borderRadius="16%"
    bottom="6rem"
    boxShadow="rgb(38, 30, 29) 0px 0px 14px 0px"
    color="#eee"
    display
    id="Button_ScrollToTop"
    onClick={() => scrollToTop(currentPage |> pageToScrollViewId)}
    /*bottom padding is lower to account for the image's extra space at bottom (arrow_up svg file)*/
    padding="0.9rem 0.9rem 0.5rem"
    position="absolute"
    right="5%"
    zIndex="1">
    <img
      alt="top"
      src=arrow_up
      style={ReactDOMRe.Style.make(~width="1.75rem", ())}
    />
  </Button>;
};
