//"inherit" is a reserved keyword & messes up the syntax highighting in Atom editor. For fontFamily prop. As of June 2019
let _inherit = "inherit";

[@react.component]
let make = /*props*/
    (
      ~alignItems: string="center",
      ~background: string="",
      ~border: string="",
      ~borderRadius: string="",
      ~borderWidth: string="",
      ~bottom: string="",
      ~boxShadow: string="none",
      ~color: string="",
      ~className: string="",
      ~display: string="flex",
      ~flex: string="",
      ~flexDirection: string="initial",
      ~fontFamily: string=_inherit,
      ~fontSize: string="",
      ~height: string="initial",
      ~id: string="",
      ~justifyContent: string="space-between",
      ~left: string="0",
      ~margin: string="0 auto",
      ~onClick: ReactEvent.Mouse.t => unit=_event => (),
      ~overflow: string="",
      ~padding: string="",
      ~position: string="",
      ~right: string="0",
      ~top: string="",
      ~transition: string="550ms cubic-bezier(0.08, 0.68, 0, 0.85)",
      ~whiteSpace: string="",
      ~width: string="",
      ~zIndex: string="",
      ~children: React.element,
    ) => {
  //------------------------
  //------------------layout
  <div
    className={className ++ " box"}
    id
    onClick
    style={ReactDOMRe.Style.make(
      ~alignItems,
      ~background,
      ~border,
      ~borderRadius,
      ~borderWidth,
      ~bottom,
      ~boxShadow,
      ~color,
      ~display,
      ~flex,
      ~flexDirection,
      ~fontFamily,
      ~fontSize,
      ~height,
      ~justifyContent,
      ~left,
      ~margin,
      ~maxWidth="700px",
      ~overflow,
      ~padding,
      ~position,
      ~right,
      ~top,
      ~transition,
      ~whiteSpace,
      ~width,
      ~zIndex,
      (),
    )}>
    children
  </div>;
};
