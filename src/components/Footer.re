[@react.component]
let make = /*props*/
    (
      ~alignItems: string="center",
      ~background: string="rgba(84, 85, 85, 1)",
      ~border: string="",
      ~borderWidth: string="",
      ~boxShadow: string="rgb(38, 30, 29) 0px 0px 45px 3px",
      ~display: string="flex",
      ~height: string="3rem",
      ~id: string="",
      ~flexDirection: string="row",
      ~fontFamily: string="",
      ~justifyContent="center",
      ~padding: string="0 6%",
      ~position: string="absolute",
      ~children: React.element,
    ) => {
  //------------------------
  //------------------layout
  <footer
    className="footer"
    id
    style={ReactDOMRe.Style.make(
      ~alignItems,
      ~background,
      ~border,
      ~borderWidth,
      ~bottom="0",
      ~boxShadow,
      ~display,
      ~flexDirection,
      ~fontFamily,
      ~fontSize="1.2rem",
      ~height,
      ~justifyContent,
      ~left="0",
      ~padding,
      ~position,
      ~right="0",
      ~transition="0s",
      ~whiteSpace="pre",
      ~zIndex="3",
      (),
    )}>
    children
  </footer>;
};
