open Helpers;
open Types;

let navToPage: (string, unit) => unit =
  (string, ()) => {
    push("/" ++ string ++ "/");
  };

//some randomly thought-out menu type
let menuItems: array(menuItem) = [|
  {color: "#ffe0a0", name: "home", page: Home},
  {color: "#ffe0a0", name: "page2", page: Page2},
|];

[@react.component]
let make = /*props*/
    (~closeMenu: unit => unit, ~isMenuOpened: bool) => {
  //-------------------------------------
  //-------------------------------layout
  <nav
    id="Navigation"
    style={ReactDOMRe.Style.make(
      ~display="flex",
      ~justifyContent="center",
      ~width="100%",
      (),
    )}>
    /*
     A menu button with an absolute positioning could be added here. Uncomment the following Button element to test it out
     <Button
        bottom="4rem"
        id="Button_OpenMenu"
        position="absolute"
        onClick=openMenu
        zIndex={isMenuOpened ? "0" : "4"}>
        {"Menu Button" |> str}
     </Button>*/

      <ul
        id="Menu"
        style={ReactDOMRe.Style.make(
          ~background="linear-gradient(to top left, #445, #444, #334",
          ~bottom="0",
          ~display="flex",
          ~flexDirection="column",
          ~justifyContent="flex-end",
          ~listStyle="none",
          ~margin="0",
          ~opacity={isMenuOpened ? "0.93" : "0"},
          ~padding="20% 0",
          ~pointerEvents={isMenuOpened ? "auto" : "none"},
          ~position="absolute",
          ~textAlign="center",
          ~top="0",
          ~transform={isMenuOpened ? "translateY(0px)" : "translateY(10px)"},
          ~transition="200ms",
          ~width="100%",
          ~zIndex={isMenuOpened ? "2" : "1"},
          (),
        )}>
        <Swipe onSwipedDown={_ => closeMenu()}>
          {menuItems
           |> Array.map(menuItem =>
                <li key={menuItem.name}>
                  <Button
                    background="none"
                    className="menuItem"
                    color={menuItem.color}
                    fontSize="3rem"
                    onClick={() =>
                      isMenuOpened
                        ? {
                          ()
                          |> /*use prop to close menu*/ closeMenu
                          |> /*navigate to page*/ navToPage(menuItem.name);
                        }
                        : /*do nothing*/ ()
                    }
                    padding="0.5rem 0"
                    pointerEvents={isMenuOpened ? "auto" : "none"}
                    tabIndex={isMenuOpened ? /*can tab*/ 0 : /*no tab*/ (-1)}
                    width="100%">
                    {menuItem.name |> str}
                  </Button>
                </li>
              )
           //a close-menu button could be inserted here
           /*<li><Button/></li>*/
           |> /*convert array to react element*/ React.array}
        </Swipe>
      </ul>
    </nav>;
};
