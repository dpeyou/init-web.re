open Types;

//for scroll-to-top button
let pageToScrollViewId: page => string =
  currentPage =>
    /*switch the target id based on the current page/route*/
    switch (currentPage) {
    | Error => "ScrollView_Error"
    | Home => "ScrollView_Home"
    | Page2 => "ScrollView_Page2"
    };

//for routing
let pageToString: page => string =
  page =>
    switch (page) {
    | Error => "error"
    | Home => "home"
    | Page2 => "page2"
    };

//for routing
let push: string => unit = ReasonReact.Router.push;

let str: string => React.element = React.string;

//get url then map it to a route/page
let urlToPage: url => page =
  url =>
    switch (url.path) {
    | [""]
    | ["home"]
    | ["page1"] => Home
    | ["page2"] => Page2
    | _ => Home
    };
