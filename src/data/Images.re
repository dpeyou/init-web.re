//app file
[@bs.module] external arrow_up: string = "../img/arrow_up.svg";
[@bs.module] external chevron_down: string = "../img/down.svg";
[@bs.module] external chevron_up: string = "../img/up.svg";
[@bs.module] external x: string = "../img/x.svg";

//collapsible
[@bs.module] external down: string = "../img/down.svg";
[@bs.module] external up: string = "../img/up.svg";
//repeated in app file[@bs.module] external x: string = "../img/x.svg";

//scroll to top button
//repeated in app file [@bs.module] external arrow_up: string = "/img/arrow_up.svg";
