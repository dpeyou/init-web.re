type menuItem = {
  color: string,
  name: string,
  page,
}
and page =
  | Error
  | Home
  | Page2;

type url = ReasonReact.Router.url;
