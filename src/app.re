open Helpers;
open Images;
open Types;

module App = {
  type state = {
    isMenuOpened: bool,
    scrollTop_Home: int,
    scrollTop_Page2: int,
    state_Home: bool /*this could be turned into type array(bool), a tuple of type (bool, bool), etc to store multiple values*/,
  };

  type action =
    | GetScrollTop(int, page)
    | ToggleItem(page, bool)
    | ToggleMenu(bool);

  [@react.component]
  let make = () /*no props*/ => {
    //set the current page using router api
    //https://reasonml.github.io/reason-react/docs/en/router
    let currentPage: page =
      ReasonReact.Router.useUrl() |> /*map url to a page*/ urlToPage;
    //-------------------------------
    //--------------start of reducer
    let (state, dispatch) =
      React.useReducer(
        (state, action) =>
          switch (action) {
          | GetScrollTop(scrollTop, page) =>
            switch (page) {
            | Home => {...state, scrollTop_Home: scrollTop}
            | Page2 => {...state, scrollTop_Page2: scrollTop}
            | _ => state
            }
          | ToggleItem(page, itemState) =>
            switch (page) {
            | Home =>
              itemState == true
                ? {...state, state_Home: false}
                : {...state, state_Home: true}
            | _ => state
            }
          | ToggleMenu(menuState) =>
            menuState == true
              ? {...state, isMenuOpened: false}
              : {...state, isMenuOpened: true}
          },
        //initial state
        {
          isMenuOpened: false,
          scrollTop_Home: 0,
          scrollTop_Page2: 0,
          state_Home: false,
        },
      );
    //----------------------------
    //--------------end of reducer

    let _onRender = {
      React.useState(() => Js.log("app has rendered"));
    };

    //------------------------
    //------------------layout
    <div
      id="App"
      style={ReactDOMRe.Style.make(
        ~background=
          "linear-gradient(to bottom right, #555a60, #62606a, #68656f)",
        ~bottom="0",
        ~left="0",
        ~margin="0 auto",
        ~maxWidth="700px",
        ~overflow="hidden",
        ~position="absolute",
        ~right="0",
        ~top="0",
        (),
      )}>
      <Header justifyContent="flex-start">
        {"current page: " |> str}
        <div className="yellowText">
          {currentPage |> pageToString |> str}
        </div>
      </Header>
      //----
      //menu
      <Menu
        closeMenu={() => dispatch(ToggleMenu(true))}
        isMenuOpened={state.isMenuOpened}
        //openMenu={() => dispatch(ToggleMenu(false))}
      />
      //------------
      //pages/routes
      {switch (currentPage) {
       | Home =>
         <Home
           onScroll={(scrollTop, ()) =>
             dispatch(GetScrollTop(scrollTop, Home))
           }
           scrollTop={state.scrollTop_Home}
           toggleCollapsible={_ =>
             dispatch(ToggleItem(Home, state.state_Home))
           }
           toggleItem={state.state_Home}
         />
       | Page2 =>
         <Page2
           onScroll={(scrollTop, ()) =>
             dispatch(GetScrollTop(scrollTop, Page2))
           }
           scrollTop={state.scrollTop_Page2}
         />
       //error page/route: do something fancy here
       | Error => <div> {"error wtf" |> str} </div>
       }}
      //--------------------
      //scroll-to-top button
      <ScrollToTop
        currentPage
        display={
          (
            switch (currentPage) {
            | Home => state.scrollTop_Home
            | Page2 => state.scrollTop_Page2
            | _ => 0
            }
          )
          > 300
            ? "block" : "none"
        }
      />
      //-----------------
      //footer/navigation
      <Footer justifyContent="space-around">
        //---------------
        //button_homepage

          <Button
            className="button_Home"
            color="#ffe0a0"
            fontSize="1.3rem"
            id="Button_HomePage"
            onClick={() => ReasonReact.Router.push("/home/")}
            padding="1.5rem">
            {"home" |> str}
          </Button>
          //-----------
          //menu button
          <Button
            className="button_Menu"
            onClick={() => dispatch(ToggleMenu(state.isMenuOpened))}
            padding="1.5rem">
            <Swipe
              onSwipedDown={_event =>
                state.isMenuOpened == true ? dispatch(ToggleMenu(true)) : ()
              }
              onSwipedUp={_event =>
                state.isMenuOpened == false
                  ? dispatch(ToggleMenu(false)) : ()
              }>
              <img
                alt="menu"
                src={state.isMenuOpened == false ? chevron_up : x}
                style={ReactDOMRe.Style.make(~width="2.2rem", ())}
              />
            </Swipe>
          </Button>
          //------------
          //page2 button
          <Button
            className="button_Page2"
            color="#ffe0a0"
            fontSize="1.3rem"
            id="Button_Page2"
            //onClick={() => updateUrl((), "page2")}
            onClick={() => ReasonReact.Router.push("/page2/")}
            padding="1.5rem">
            {"page2" |> str}
          </Button>
        </Footer>
    </div>;
  };
};
//render this app to HTML file at div with ID "AppContainer"
ReactDOMRe.renderToElementWithId(<App />, "AppContainer");
