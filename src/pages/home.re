open Helpers;

[@react.component]
let make = /*~props*/
    (
      ~onScroll: (int, unit) => unit,
      ~scrollTop: int,
      ~toggleCollapsible: unit => unit,
      ~toggleItem: bool,
    ) => {
  //------------------------
  //------------------layout
  <ScrollView id="ScrollView_Home" onScroll scrollTop>
    <p
      style={ReactDOMRe.Style.make(
        ~lineHeight="1.55",
        ~margin="2.5rem 5% 2rem",
        (),
      )}>
      {"Here you go, another template. This one is a reference point, with the typical things I need to get started writing web apps in"
       |> str}
      <a
        href="https://reasonml.github.io/"
        style={ReactDOMRe.Style.make(
          ~color="#ffb080",
          ~textDecoration="underline",
          (),
        )}
        target="_blank">
        {" ReasonML." |> str}
      </a>
      <br />
      {"The project is compiled to JavaScript using " |> str}
      <a
        href="https://bucklescript.github.io/"
        style={ReactDOMRe.Style.make(
          ~color="violet",
          ~textDecoration="underline",
          (),
        )}
        target="_blank">
        {" BuckleScript." |> str}
      </a>
      <br />
      <a
        href="https://gitlab.com/dpeyou/init-web.re"
        style={ReactDOMRe.Style.make(
          ~alignSelf="flex-end",
          ~color="#a0ffa0",
          ~float="right",
          ~marginBottom="2.5rem",
          ~textDecoration="underline",
          (),
        )}
        target="_blank">
        {"View code" |> str}
      </a>
    </p>
    <Collapsible
      buttonFontSize="1.25rem"
      buttonText_Closed="Don't click this..."
      buttonText_Opened="Secret text revealed"
      justifyButtonText="space-between"
      margin="2rem auto"
      onClick=toggleCollapsible
      showContent=toggleItem
      /*copy-paste some "lorem ipsum"*/
      text=" Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin semper eros odio, non interdum felis dignissim sagittis. Integer lobortis est rhoncus fermentum sollicitudiLorem ipsum dolor sit amet, consectetur adipiscing elit. Proin semper eros odio, non interdum felis dignissim sagittis. Integer lobortis est rhoncus fermentum sollicitudin. Aenean Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin semper eros odio, non interdum felis dignissim sagittis. Integer lobortis est rhoncus fermentum sollicitudin. Aenean volutpat vitae magna in faucibus. Morbi lobortis, risus ac venenatis imperdiet, metus sapien vulputate elit, eu auctor dui turpis sit amet ante. Donec consequat congue urna, ac dapibus enim euismod in. Sed at nulla turpis vitae magna in faucibus. Morbi lobortis, risus ac venenatis imperdiet, metus sapien vulputate elit, eu auctor dui turpis sit amet ante. Donec consequat congue urna, ac dapibus enim euismod in. Sed at nulla turpisn. Aenean volutpat vitae magna in faucibus. Morbi lobortis, risus ac venenatis imperdiet, metus sapien vulputate elit, eu auctor dui turpis sit amet ante. Donec consequat congue urna, ac dapibus enim euismod in. Sed at nulla turpiLorem ipsum dolor sit amet, consectetur adipiscing elit. Proin semper eros odio, non interdum felis dignissim sagittis. Integer lobortis est rhoncus fermentum sollicitudin. Aenean volutpat vitae magna in faucibus. Morbi lobortis, risus ac venenatis imperdiet, metus sapien vulputate elit, eu auctor dui turpis sit amet ante. Donec consequat congue urna, ac dapibus enim euismod in. Sed at nulla turpiss."
    />
  </ScrollView>;
};
