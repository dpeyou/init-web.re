[@react.component]
let make = /*~props*/
    (~onScroll: (int, unit) => unit, ~scrollTop: int) => {
  //------------------------
  //------------------layout
  <ScrollView id="ScrollView_Page2" onScroll scrollTop>
    <p style={ReactDOMRe.Style.make(~margin="2.5rem 1.5rem 2rem", ())}>
      {"2nd page" |> React.string}
    </p>
  </ScrollView>;
};
