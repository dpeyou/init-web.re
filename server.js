const Express = require('express')
const Server = Express()
const Port = process.env.PORT || 4000

//route requests for static files to appropriate directory
Server.use('/', Express.static(__dirname + '/w'))

//other routes defined before catch-all
/*
Server.get('/some-route', (req, res) => {
  res.send('ok')
})
*/

//catch all the other routes using asterisk ( * )
Server.use('/*', Express.static(__dirname + '/w'))

//start server
Server.listen(Port, function() {
  console.log('server listening on port ' + Port)
})
